# Instrucciones para crear la base de de datos SIA

* Crear directorio csv `mkdir csv`
* Crear un archivo header.csv con el contenido de las cabeceras de la base de
datos, por ejemplo: `nombre,documento,plan,correo,curso,grupo,equipo`
* Descargar la base de datos en formato csv y guardar por nombre y grupo, por
ejemplo curso-analoga-g1.csv, curso-taller-g1.csv, curso-digital-g1.csv, el contenido debe coincidir con el propuesto en header.csv
* Guardar los archivos xls como archivos csv "Save as" delimitador "," y para los string las comillas dobles
* Ejecutar el comando make para crear las bases de datos `csv/notas.csv`
* Puede ver el resultado final con `make csv-view`
* Remover la base de datos de notas.sqlite3 o borrar su contenido con `make remove-table-students remove-table-notas`
* Iniciar el proceso de creación de base de datos desde el csv con `make create-table-students create-table-notas`
* Insertar estudiantes con `make insert-students`
* Puede ver la lista de estudiantes con `make sqliteproject`


* En el Makefile configurar las bariables de SERVER, PORT_SSH y USER_SSH
* Ahora, guarda el pass del ssh en un archivo llamado "pass" el cual no debe ser sincronizado al repositorio de git
* Inicia el servicio ssh e instala las herramientas para discord
* Ahora envia la base de datos junto a los scripts de discord y flask `make send-py-db-ssh`
* Pruebe realizar una copia de su base de datos local con `make receive-db-ssh`

