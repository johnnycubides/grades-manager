Buscar por grupo

```sql
/* Selección por grupo */
SELECT estudiantes.documento, estudiantes.grupo, notas.id_actividad, notas.descripcion, notas.nota
FROM  estudiantes 
JOIN notas  ON estudiantes.documento = notas.documento
WHERE estudiantes.grupo = 6;
```

Buscar estudiante

```sql
/* Selección por grupo */
SELECT estudiantes.documento, estudiantes.grupo, notas.id_actividad, notas.descripcion, notas.nota
FROM  estudiantes 
JOIN notas  ON estudiantes.documento = notas.documento
WHERE estudiantes.documento = 1028663102;

```

Notas final grupo
```sql
SELECT
    estudiantes.nombre,
    estudiantes.grupo,
    estudiantes.documento,
    CASE
        -- Fórmula para el grupo 1: ((taller1 + taller2) * 0.2) + examen1 * 0.4 + examen2 * 0.4
        WHEN estudiantes.grupo = 2 THEN 
            ((SUM(CASE WHEN notas.id_actividad = 'Taller 1' THEN notas.nota ELSE 0 END) +
			SUM(CASE WHEN notas.id_actividad = 'Taller 2' THEN notas.nota ELSE 0 END) +
			SUM(CASE WHEN notas.id_actividad = 'Lab 1' THEN notas.nota ELSE 0 END) +
			SUM(CASE WHEN notas.id_actividad = 'Lab 2' THEN notas.nota ELSE 0 END) +
			SUM(CASE WHEN notas.id_actividad = 'Lab 3' THEN notas.nota ELSE 0 END)) * 0.2/5) +
             SUM(CASE WHEN notas.id_actividad = 'Ideacion' THEN notas.nota ELSE 0 END) * 0.1 +
			 SUM(CASE WHEN notas.id_actividad = 'Implementacion' THEN notas.nota ELSE 0 END) * 0.4 +
			 SUM(CASE WHEN notas.id_actividad = 'Documentacion' THEN notas.nota ELSE 0 END) * 0.2 +
             SUM(CASE WHEN notas.id_actividad = 'Ingenia' THEN notas.nota ELSE 0 END) * 0.1
			 
        WHEN estudiantes.grupo = 3 THEN 
            ((SUM(CASE WHEN notas.id_actividad = 'Taller 1' THEN notas.nota ELSE 0 END) +
			SUM(CASE WHEN notas.id_actividad = 'Taller 2' THEN notas.nota ELSE 0 END) +
			SUM(CASE WHEN notas.id_actividad = 'Lab 1' THEN notas.nota ELSE 0 END) +
			SUM(CASE WHEN notas.id_actividad = 'Lab 2' THEN notas.nota ELSE 0 END) +
			SUM(CASE WHEN notas.id_actividad = 'Lab 3' THEN notas.nota ELSE 0 END) +
			SUM(CASE WHEN notas.id_actividad = 'Lab 4' THEN notas.nota ELSE 0 END)) * 0.2/6) +
             SUM(CASE WHEN notas.id_actividad = 'Ideacion' THEN notas.nota ELSE 0 END) * 0.1 +
			 SUM(CASE WHEN notas.id_actividad = 'Implementacion' THEN notas.nota ELSE 0 END) * 0.4 +
			 SUM(CASE WHEN notas.id_actividad = 'Documentacion' THEN notas.nota ELSE 0 END) * 0.2 +
             SUM(CASE WHEN notas.id_actividad = 'Ingenia' THEN notas.nota ELSE 0 END) * 0.1

        WHEN estudiantes.grupo = 6 THEN 
            ((SUM(CASE WHEN notas.id_actividad = 'Taller 1' THEN notas.nota ELSE 0 END) +
			SUM(CASE WHEN notas.id_actividad = 'Taller 2' THEN notas.nota ELSE 0 END) +
			SUM(CASE WHEN notas.id_actividad = 'Taller 3' THEN notas.nota ELSE 0 END) +
			SUM(CASE WHEN notas.id_actividad = 'Lab 1' THEN notas.nota ELSE 0 END) +
			SUM(CASE WHEN notas.id_actividad = 'Lab 2' THEN notas.nota ELSE 0 END) +
			SUM(CASE WHEN notas.id_actividad = 'Lab 3' THEN notas.nota ELSE 0 END) +
			SUM(CASE WHEN notas.id_actividad = 'Lab 4' THEN notas.nota ELSE 0 END)) * 0.2/7) +
             SUM(CASE WHEN notas.id_actividad = 'Ideacion' THEN notas.nota ELSE 0 END) * 0.1 +
			 SUM(CASE WHEN notas.id_actividad = 'Implementacion' THEN notas.nota ELSE 0 END) * 0.4 +
			 SUM(CASE WHEN notas.id_actividad = 'Documentacion' THEN notas.nota ELSE 0 END) * 0.2 +
             SUM(CASE WHEN notas.id_actividad = 'Ingenia' THEN notas.nota ELSE 0 END) * 0.1
			 
        WHEN estudiantes.grupo = 7 THEN 
            ((SUM(CASE WHEN notas.id_actividad = 'Taller 1' THEN notas.nota ELSE 0 END) +
			SUM(CASE WHEN notas.id_actividad = 'Taller 2' THEN notas.nota ELSE 0 END) +
			SUM(CASE WHEN notas.id_actividad = 'Lab 1' THEN notas.nota ELSE 0 END) +
			SUM(CASE WHEN notas.id_actividad = 'Lab 2' THEN notas.nota ELSE 0 END) +
			SUM(CASE WHEN notas.id_actividad = 'Lab 3' THEN notas.nota ELSE 0 END) +
			SUM(CASE WHEN notas.id_actividad = 'Lab 4' THEN notas.nota ELSE 0 END)) * 0.2/6) +
             SUM(CASE WHEN notas.id_actividad = 'Ideacion' THEN notas.nota ELSE 0 END) * 0.1 +
			 SUM(CASE WHEN notas.id_actividad = 'Implementacion' THEN notas.nota ELSE 0 END) * 0.4 +
			 SUM(CASE WHEN notas.id_actividad = 'Documentacion' THEN notas.nota ELSE 0 END) * 0.2 +
             SUM(CASE WHEN notas.id_actividad = 'Ingenia' THEN notas.nota ELSE 0 END) * 0.1

        -- Fórmula para el grupo 2: ((taller1 + lab1) * 0.2) + examen1 * 0.4 + examen2 * 0.4
        /*WHEN e.grupo = 3 THEN 
            ((SUM(CASE WHEN a.actividad = 'taller1' THEN a.nota ELSE 0 END) +
              SUM(CASE WHEN a.actividad = 'lab1' THEN a.nota ELSE 0 END)) * 0.2) +
             SUM(CASE WHEN a.actividad = 'examen1' THEN a.nota ELSE 0 END) * 0.4 +
             SUM(CASE WHEN a.actividad = 'examen2' THEN a.nota ELSE 0 END) * 0.4

        -- Fórmula para el grupo 3: ((taller2 + lab2) * 0.2) + examen1 * 0.4 + examen2 * 0.4
        WHEN e.grupo = 6 THEN 
            ((SUM(CASE WHEN a.actividad = 'taller2' THEN a.nota ELSE 0 END) +
              SUM(CASE WHEN a.actividad = 'lab2' THEN a.nota ELSE 0 END)) * 0.2) +
             SUM(CASE WHEN a.actividad = 'examen1' THEN a.nota ELSE 0 END) * 0.4 +
             SUM(CASE WHEN a.actividad = 'examen2' THEN a.nota ELSE 0 END) * 0.4
			 */
    END AS nota_final
FROM estudiantes
JOIN notas ON estudiantes.documento = notas.documento
GROUP BY estudiantes.nombre, estudiantes.grupo, estudiantes.documento
ORDER BY estudiantes.grupo, estudiantes.nombre;
```
