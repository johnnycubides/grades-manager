import sqlite3

DATABASE = './notas.sqlite3'

from flask import Flask, request, jsonify, send_file
from flask_cors import CORS

app = Flask(__name__)
CORS(app)

@app.route('/query', methods=['POST'])
def execute_query():
    # print("Inicia query")
    query = request.json.get('query')
    # print(query)
    if not query:
        print("Formato no válido del query")
        return jsonify({'error': 'Se requiere una consulta SQL'}), 400

    try:
        connection = sqlite3.connect(DATABASE)
        cursor = connection.cursor()
        cursor.execute(query)
        result = cursor.fetchall()
        # print(result)
        connection.commit()
        connection.close()
        return jsonify({'result': result}), 200
    except Exception as e:
        # print("Error")
        return jsonify({'error': str(e)}), 500

@app.route('/descargar', methods=['GET'])
def get_file():
    path_file = './notas.sqlite3'
    try:
        return send_file(path_file, as_attachment=True)
    except FileNotFoundError:
        return 'El archivo no se encontró', 404

if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=False)
