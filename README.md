# discord-bot-python-sqlite3

## Instalación de dependencias

1. Instalar dependencias

```bash
sudo apt update
sudo apt install python3-pip
sudo apt install python3-venv
sudo apt install htop # Para manejo de tareas por ese cliente
```

2. Crear variable de entorno

```bash
python3 -m venv dev
```
3. Instalar librerías con pip

```bash
dev/bin/pip install discord.py
dev/bin/pip install --upgrade watchdog
dev/bin/pip install Flask
dev/bin/pip install flask_cors
```

4. Lanzar servicios

```
cd discord-bot
../dev/bin/python3 flask_server.py &
../dev/bin/python3 samus-discord.py &

```

## Comamdos útiles

Traer achivos desde el server por ssh
scp user@dominio:/root/directorio/archivo ./

Enviar archivos al servidor
scp archivo user@dominio:/root/directorio/

Instalación de discord-python
`pip3 install -U discord.py`

Crear base de datos sqlite3 desde un archivo csv

doc: https://csvkit.readthedocs.io/en/1.0.2/scripts/in2csv.html

```bash
sudo pip install csvkit
```

```sh
csvsql --db sqlite:///chicas.sqlite3 --tables chicas --insert chicasbase.csv
csvsql --db sqlite:///chicas-sed.sqlite3 --tables chicas_sed --insert chicasbase-sed.csv
```

browser sqlite
```bash
sudo apt install sqlitebrowser
```

Iniciar en el servidor el script:
```bash
python3 discord-bot-chicas2022.py &
```

## Proceso

1. Descargar base de datos en archivo separado por comas CSV
2. Dejar solamente las columnas siguientes: SALA, DOC, NOMBRES, APELLIDOS, Correo
3. Cambiar el nombre del archivo CSV por BT-NINAS-2022.csv y guardar en directorio csv/
4. Limpiar la base de datos `make clean`
5. Exportar base de datos, csv to db sqlite `make chicas-db`
6. Entrar a sqlitebrowser `make sqlitebrowswe` verificar/cambiar el tipo de todas las columnas de la tabla chicas por el tipo VARCHAR
7. Enviar la base de datos al servidor `make send-db-ssh` 
8. Detener el script de python con htop
9. Reiniciar el script de python `root@dev:~/discord-bot# python3 discord-bot-chicas2022.py &` 
10. Comprobar el funcionamiento


## Refeencias

[Example in github](https://github.com/Rapptz/discord.py/tree/v1.7.3/examples)

[Documentación de discord en python](https://discordpy.readthedocs.io/en/stable/index.html)
