import { fileURLToPath, URL } from 'node:url'

import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'

function useCredentials() {
  return {
    name: 'use-credentials',
    transformIndexHtml(html: string) {
      return html.replace(
        'crossorigin',
        'crossorigin="use-credentials"',
      );
    },
  };
}

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue(),
    useCredentials(),
  ],
  resolve: {
    alias: {
      '@': fileURLToPath(new URL('./src', import.meta.url))
    }
  }
})
