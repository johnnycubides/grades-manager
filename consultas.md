# Peticiones sql


```sql
INSERT INTO notas (documento, id_nota, nota ) VALUES (1123436019, CURRENT_TIMESTAMP, 5.0);
INSERT INTO notas (timestamp, documento, id_actividad, descripcion, nota ) VALUES (CURRENT_TIMESTAMP, "1123436019", "lab1", "Descripción", 5.0);
```

```sql
SELECT notas.*
FROM estudiantes
INNER JOIN notas ON estudiantes.documento = notas.documento
WHERE estudiantes.documento = 'documento_del_estudiante';
```
