import logging, sys
import random
import sqlite3
import discord
from discord.ext import commands

logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)

conn = sqlite3.connect('./notas.sqlite3')
# conn_sed = sqlite3.connect('./chicas-sed.sqlite3')

class MyContext(commands.Context):
    async def welcome(self, channelWelcome):
        try:
            await channelWelcome.send('@here :sunflower: :hugging:  Bienvenidas al servidor de Chicas STEAM Colombia :unicorn: :telescope:\nPara darte acceso al contenido tendrás que escribir el comando **$chica** dejando un espacio y seguido escribes tu número de identificación sin puntos ni comas, ejemplo:\n**$chica 102030405060**')
        except discord.HTTPException:
            pass
    async def tick(self, value):
        # reacts to the message with an emoji
        # depending on whether value is True or False
        # if its True, it'll add a green check mark
        # otherwise, it'll add a red cross mark
        emoji = '\N{WHITE HEAVY CHECK MARK}' if value else '\N{CROSS MARK}'
        try:
            # this will react to the command author's message
            await self.message.add_reaction(emoji)
        except discord.HTTPException:
            # sometimes errors occur during this, for example
            # maybe you dont have permission to do that
            # we dont mind, so we can just ignore them
            pass
    async def msg2result(self, value, channelReport):
        try:
            cur = conn.cursor()
            # Se ajusta el comando de busqueda a través de sqlitebrowser
            cur.execute('SELECT "DOC", "NOMBRES", "APELLIDOS", "Correo", "SALA" FROM chicas WHERE "DOC"='+value)
            chicas = cur.fetchall()
            chica = []
            logging.debug(chicas)
            # logging.debug(chicas[0] if chicas[0] != None else 'Vacio')
            await self.message.delete()
            if len(chicas) == 1:
                chica = list(chicas[0])
                logging.debug("IDENTIFICADA NAL")
                chica[0] = chica[0] if chica[0] != None else "Sin datos" # DOC
                chica[1] = chica[1] if chica[1] != None else "Sin datos" # NOMBRES
                chica[2] = chica[2] if chica[2] != None else "Sin datos" # APELLIDOS
                chica[3] = chica[3] if chica[3] != None else "Sin datos" # Correo
                chica[4] = chica[4] if chica[4] != None else "Sin datos" # SALA
                logging.debug(chica)
                message2mediador = "<@" + str(self.message.author.id) + ">\n"+chica[1]+" "+chica[2]+"\nDocumento: "+chica[0]+"\nSala: "+chica[4]+", Correo: "+chica[3]
                logging.debug(message2mediador)
                await channelReport.send(message2mediador)
                if chica[4] != "Sin datos":
                    await self.message.channel.send("<@" + str(self.message.author.id) + ">\n Hola "+chica[1]+" has sido identificada, tu mediador te dará acceso al contenido. ¡Disfruta del contenido!")
                else:
                    await self.message.channel.send("<@" + str(self.message.author.id) + ">\n Hola "+chica[1]+" el bot no pudo identificar tu grupo, reporta esta situación a tu mediador para poder actualizar tus datos.")
            else:
                await self.message.channel.send("<@" + str(self.message.author.id) + ">\n No te hemos podido identificar a través de tu documento de identidad, ten en cuenta que debes escribir el número sin puntos y comas, por favor vuelve a intentarlo, sino funciona, envíale un mensaje directo a tu mediador (son los usuarios que tienen el nombre de color amarillo).")
            # print(self.message.author)
            # print(self.message.author.id)
        except discord.HTTPException:
            pass

class MyBot(commands.Bot):
    async def get_context(self, message, *, cls=MyContext):
        # when you override this method, you pass your new Context
        # subclass to the super() method, which tells the bot to
        # use the new MyContext class
        return await super().get_context(message, cls=cls)

bot = MyBot(command_prefix='$')

@bot.event
async def on_ready():
    bot.channelReport = bot.get_channel(877314284126863360)
    bot.channelWelcome = bot.get_channel(869727930366980107)
    # bot.channelReport = bot.get_channel(869727930790576194)

# @client.event
# async def on_member_join(member):
#     print("memeber join")
#     # print("Recognized that " + member.name + " joined")
#     # await bot.send_message(member, newUserDMMessage)
#     # await bot.send_message(discord.Object(id='CHANNELID'), 'Welcome!')
#     # print("Sent message to " + member.name)
#     # print("Sent message about " + member.name + " to #CHANNEL")
#     await bot.channelWelcome.send("hola "+member)

@bot.command()
async def guess(ctx, number: int):
    """ Guess a random number from 1 to 6. """
    # explained in a previous example, this gives you
    # a random number from 1-6
    value = random.randint(1, 6)
    # with your new helper function, you can add a
    # green check mark if the guess was correct,
    # or a red cross mark if it wasnt
    await ctx.tick(number == value)

@bot.command()
async def chica(ctx, number: str):
    await ctx.msg2result(number, bot.channelReport)

@bot.command() # Hay chicas que escriben el comando chicas
async def chicas(ctx, number: str):
    await ctx.msg2result(number, bot.channelReport)

@bot.command() # Hay chicas que escriben el comando Chica
async def Chica(ctx, number: str):
    await ctx.msg2result(number, bot.channelReport)

@bot.command()
async def hola(ctx):
    await ctx.welcome(bot.channelWelcome)

# Analizador de error para corregir comando
@bot.event
async def on_command_error(ctx, error):
    logging.debug("SE HA DISPARADO ERROR EN EL COMANDO")
    # print(help(ctx.message))
    # print(ctx.command_failed)
    # print(ctx.message.content)
    msg = ctx.message.content
    if(msg.find("$chicas")>=0):
        await ctx.msg2result(msg.replace("$chicas", ""), bot.channelReport)
    elif(msg.find("$chica")>=0):
        await ctx.msg2result(msg.replace("$chica", ""), bot.channelReport)
    # TODO se requiere un mensaje adicional cuando la chica haga uso de un comando que no es reconocido

# important: you shouldnt hard code your token
# these are very important, and leaking them can 
# let people do very malicious things with your
# bot. try to use a file or something to keep
# them private, and dont commit it to GitHub
token = "ODc3MTkzMjcwMzk1MDM5ODE2.YRvD5Q.BD88vMcnKQHaGWWIS247Rut3qXA"
bot.run(token)
