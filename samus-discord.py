import sqlite3
import logging, sys
import random
import discord
from discord.ext import commands

# load_dotenv()
# TOKEN = os.getenv('DISCORD_TOKEN')

logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)

DATABASE = './notas.sqlite3'

# MyContext hace referencia a las funciones que serán asociadas a los comandos
class MyContext(commands.Context):
    async def notas(self, value):
        try:
            query = 'SELECT notas.timestamp, notas.documento, notas.id_actividad, notas.nota, notas.descripcion  FROM estudiantes INNER JOIN notas ON estudiantes.documento = notas.documento WHERE estudiantes.documento = ' + value +';'
            connection = sqlite3.connect(DATABASE)
            cursor = connection.cursor()
            cursor.execute(query)
            grades = cursor.fetchall()
            # Borrar el mensaje de la solicitud
            # await self.message.delete()
            await self.message.channel.send('Para el documento consultado No **' + value + '**, estas son las actividades reportadas:')
            number = 0
            msg2send = ""
            for grade in grades:
                # print(grade[4])
                number += 1
                msg2send += str(number)+'. **' + str(grade[2]) + '**' + ', nota: ' + str(grade[3]) + ', entregada el ' + str(grade[0]) + ', ' + str(grade[4]) + '\n'
                if number % 10 == 0 :
                    await self.message.channel.send(msg2send)
                    msg2send = ""
            if msg2send != "":
                await self.message.channel.send(msg2send)
            # for grade in grades:
            #     msg = 'Actividad: ' + grade[2] + 'Nota: ' + grade[4]
            #     await self.message.channel.send(grade)
        except discord.HTTPException:
            pass

# Definiendo la clase Bot que requerirá de el contexto
class MyBot(commands.Bot):
    async def get_context(self, message, *, cls=MyContext):
        # when you override this method, you pass your new Context
        # subclass to the super() method, which tells the bot to
        # use the new MyContext class
        return await super().get_context(message, cls=cls)


description = "Discord bot"

intents = discord.Intents.default()
intents.members = True
intents.message_content = True

# bot = commands.Bot(command_prefix='?', description=description, intents=intents)
bot = MyBot(command_prefix='!', description=description, intents=intents)
# bot = MyBot(command_prefix='$', intents=intents)

@bot.command()
async def notas(ctx, number: str):
    await ctx.notas(number)

@bot.event
async def on_ready():
    print(f'Logged in as {bot.user} (ID: {bot.user.id})')
    print('------')

# TOKEN = "ODc3MTkzMjcwMzk1MDM5ODE2.YRvD5Q.BD88vMcnKQHaGWWIS247Rut3qXA"
TOKEN = "ODc3MTkzMjcwMzk1MDM5ODE2.GWfJce.6MbyFYuELkt9ihUi7e3mkSmQdKMI3bAFVa1uFo"
bot.run(TOKEN)
