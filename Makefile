.ONESHELL:

SHELL=/bin/bash

# Enviroment conda to activate
ENV=work
CONDA_ACTIVATE = source $$($$CONDA_EXE info --base)/etc/profile.d/conda.sh ; conda activate; conda activate $(ENV)

DB_NAME=notas
DB_EXTENSION=.sqlite3
DB=$(DB_NAME)$(DB_EXTENSION)
PYTHON_SCRIPT=./flask_server.py ./samus-discord.py
CSVFILE=./csv/$(DB_NAME).csv

#SERVER=suite.catalejoplus.com
SERVER=45.236.130.101

USER_SSH=root
# USER_SSH=admin
# USER_SSH=johnnycubides

# PORT_SSH=22
# PORT_SSH=22222
PORT_SSH=33648

ifeq ($(USER_SSH), root)
DIR=/$(USER_SSH)/discord-bot/
else
DIR=/home/$(USER_SSH)/discord-bot/
endif
# OJO no usar la palabra PATH en el Makefile ya que genera la alerta de permisos de ejecución

help: ## Prints help for targets with comments
	@cat $(MAKEFILE_LIST) | grep -E '^[a-zA-Z_-]+:.*?## .*$$' | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

csv:
	# Crear cabecera desde csv/header.csv
	@cat csv/header.csv > csv/$(DB_NAME).csv
	# Agregar estudiantes desde csv/tg2.csv
	@sed '1d' csv/curso-analoga-g5.csv >> csv/$(DB_NAME).csv # Quitar primera línea del csv y agregar
	@sed '1d' csv/curso-analoga-g6.csv >> csv/$(DB_NAME).csv # Quitar primera línea del csv y agregar
	@sed '1d' csv/curso-digital-g3.csv >> csv/$(DB_NAME).csv # Quitar primera línea del csv y agregar
	@sed '1d' csv/curso-digital-g4.csv >> csv/$(DB_NAME).csv # Quitar primera línea del csv y agregar
	@sed '1d' csv/curso-taller-g2.csv >> csv/$(DB_NAME).csv # Quitar primera línea del csv y agregar
	@sed '1d' csv/curso-taller-g3.csv >> csv/$(DB_NAME).csv # Quitar primera línea del csv y agregar
	@sed '1d' csv/curso-taller-g6.csv >> csv/$(DB_NAME).csv # Quitar primera línea del csv y agregar
	@sed '1d' csv/curso-taller-g7.csv >> csv/$(DB_NAME).csv # Quitar primera línea del csv y agregar
	# @sed '1d' csv/tg2.csv >> csv/$(DB_NAME).csv # Quitar primera línea del csv y agregar
	# @sed '1d' csv/tg3.csv >> csv/$(DB_NAME).csv # Quitar primera línea del csv y agregar
	# @sed '1d' csv/tg6.csv >> csv/$(DB_NAME).csv # Quitar primera línea del csv y agregar
	# @sed '1d' csv/tg7.csv >> csv/$(DB_NAME).csv # Quitar primera línea del csv y agregar
	# @sed '1d' csv/dg7.csv >> csv/$(DB_NAME).csv # Quitar primera línea del csv y agregar
	# @sed '1d' csv/dg8.csv >> csv/$(DB_NAME).csv # Quitar primera línea del csv y agregar

csv-view:
	less csv/$(DB_NAME).csv

# Tabla
TABLE=estudiantes

create-table-students:
	sqlite3 $(DB) <<EOF
	CREATE TABLE $(TABLE) (
			nombre VARCHAR,
			documento INTEGER PRIMARY KEY,
			plan VARCHAR,
			correo VARCHAR,
			curso VARCHAR,
			grupo INTEGER,
			equipo INTEGER
	);
	EOF

remove-table-students:
	sqlite3 $(DB) <<EOF
	DROP TABLE $(TABLE);
	EOF

insert-students:
	csvstack $(CSVFILE) | csvsql --db sqlite:///$(DB) --tables $(TABLE) --insert --no-create

create-table-notas:
	sqlite3 $(DB) <<EOF
	CREATE TABLE notas (
			timestamp VARCHAR,
			documento TEXT,
			id_actividad TEXT, -- agregar PRIMARY KEY en caso de que solo pueda haber una entrega
			nota DECIMAL(2,1), -- 2 numeros, 1 de ellos pueden ser decimales
			descripcion TEXT,
			FOREIGN KEY (documento) REFERENCES Estudiantes(documento)
			CHECK (nota >= 0 AND nota <= 5) -- Restricción nota de 0 a 5
	);
	EOF

remove-table-notas:
	sqlite3 $(DB) <<EOF
	DROP TABLE notas;
	EOF

sqliteproject:
	sqlitebrowser $(DB_NAME).sqbpro

sqlitebrowser:
	sqlitebrowser $(DB)

# generate-db:
# 	csvsql --db sqlite:///$(DB_NAME).sqlite3 --tables $(TABLE) --insert $(CSVFILE)

# overwrite-db:
# 	csvsql --db sqlite:///$(DB_NAME).sqlite3 --tables $(TABLE) --insert $(CSVFILE) > $(DB_NAME).sql
	# sqlite3 $(DB_NAME).sqlite3 < $(DB_NAME).sql
	# mv $(DB_NAME) db/

# overwrite-db:
# 	csvsql --db sqlite:///$(DB_NAME) --tables $(TABLE) --overwrite --insert $(CSVFILE)
# 	mv $(DB_NAME) db/

	# mv $(DB) db/

# overwrite-data:
# 	csvstack $(CSVFILE) | csvsql --db sqlite:///$(DB) --tables $(TABLE) --overwrite --insert


# clean:
# 	rm -rf db/*

ssh:
	ssh -p $(PORT_SSH) $(USER_SSH)@$(SERVER)
	# ssh $(USER_SSH)@$(SERVER)

send-db-ssh:
	scp -P $(PORT_SSH) $(DB) $(USER_SSH)@$(SERVER):$(DIR)
	# scp $(DB) $(USER_SSH)@$(SERVER):$(DIR)

receive-db-ssh:
	scp -P $(PORT_SSH) $(USER_SSH)@$(SERVER):$(DIR)/$(DB) . && \
	mkdir -p db && \
	cp $(DB) ./db/$(DB_NAME)$(shell date +'-%y-%m-%d-%H:%M:%S')$(DB_EXTENSION)

send-py-ssh:
	# scp -P $(PORT_SSH) $(PYTHON_SCRIPT) $(USER_SSH)@$(SERVER):$(DIR)
	scp -P $(PORT_SSH) $(PYTHON_SCRIPT) $(USER_SSH)@$(SERVER):$(DIR)

send-py-db-ssh:
	scp -P $(PORT_SSH) ./notas.sqlite3 $(PYTHON_SCRIPT) $(USER_SSH)@$(SERVER):$(DIR)

.PHONY: csv
